package com.mehmet6parmak.training.taskandbackstack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Main Activity");
    }

    public void search(View view) {
        EditText edtSearch = (EditText) findViewById(R.id.edt_search);

        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        intent.putExtra(SearchActivity.EXTRA_SEARCH_TEXT, edtSearch.getText().toString());
        startActivity(intent);
    }


    public void launchSecondActivity(View view) {
        startActivity(new Intent(this, SecondActivity.class));
    }

    public void launchNewTask(View view) {
        //in xml, it is documentLaunchMode
        Intent intent = new Intent(this, SecondActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);
    }
}
