package com.mehmet6parmak.training.taskandbackstack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SearchActivity extends AppCompatActivity {

    public static final String EXTRA_SEARCH_TEXT = "search-text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getSupportActionBar().setTitle(getIntent().getStringExtra(EXTRA_SEARCH_TEXT));
    }

    public void search(View view) {
        EditText edtSearch = (EditText) findViewById(R.id.edt_search);

        Intent intent = new Intent(SearchActivity.this, SearchActivity.class);
        intent.putExtra(SearchActivity.EXTRA_SEARCH_TEXT, edtSearch.getText().toString());
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        getSupportActionBar().setTitle(intent.getStringExtra(EXTRA_SEARCH_TEXT));
    }
}
